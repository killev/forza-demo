//
//  ForzaDemoUITests.swift
//  ForzaDemoUITests
//
//  Created by Peter Ovchinnikov on 8/30/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import XCTest

import Quick
import Nimble

import Swinject
import SwinjectAutoregistration
import ReaktiveBoar
import BrightFutures
import ReactiveKit
import Bond

struct ForzaDemoTestApp {
}
struct Onboarding {
    var nextButton: XCUIElement {
        return XCUIApplication().buttons["NEXT"]
    }
    var registerButton: XCUIElement {
        return XCUIApplication().buttons["REGISTER"]
    }
    var firstName: XCUIElement {
        return XCUIApplication().collectionViews.textFields["First Name"]
    }
    var lastName: XCUIElement {
        return XCUIApplication().collectionViews.textFields["Last Name"]
    }
    var phone: XCUIElement {
        return XCUIApplication().collectionViews.textFields["(12) 345-67-89"]
    }

    var city: XCUIElement {
        return XCUIApplication().collectionViews.buttons["CityCellButton"]
    }

    var email: XCUIElement {
        return XCUIApplication().collectionViews.textFields["user@email.com"]
    }

    var password: XCUIElement {
        return XCUIApplication().collectionViews.secureTextFields["Password"]
    }

    var confirmPassword: XCUIElement {
        return XCUIApplication().collectionViews.secureTextFields["Confirm password"]
    }

    var profile: XCUIElement {
       return XCUIApplication().collectionViews.buttons["ProfileImageCellSelectionButton"]
    }

    var aboutNote: XCUIElement {
        return XCUIApplication().collectionViews.textViews["AboutNote"]
            .existed()
    }
}

enum ImagePickerSource: String {
    case moments = "Moments"
}
struct ImagePicker {
    private var photosPage: XCUIElement {
        return XCUIApplication().navigationBars["Photos"]
    }

    private func imagesPage(source: ImagePickerSource) -> XCUIElement {
        return XCUIApplication().navigationBars[source.rawValue]
    }

    private func sourceCell(source: ImagePickerSource) -> XCUIElement {
        return XCUIApplication().cells[source.rawValue]
    }
    private func image(at: Int) -> XCUIElement {
        return XCUIApplication().collectionViews.cells.element(boundBy: at * 2 + 1)
    }

    func selectImage(source: ImagePickerSource, at: Int) {
        expect(self.photosPage.waitForExistence(timeout: 3))
             == true
        sourceCell(source: .moments).tap()
        expect(self.imagesPage(source: source).waitForExistence(timeout: 3))
             == true
        image(at: 1).tap()
    }
}

struct Alerts {
    func selectOption(option: String = "Library") {
        XCUIApplication().alerts["Select"].existed().buttons[option].tap()
    }
}

struct Tabs {
    var tournamentTab: XCUIElement {
        return XCUIApplication().tabBars.buttons["Tournament"].existed()
    }

    var profileTab: XCUIElement {
        return XCUIApplication().tabBars.buttons["Profile"].existed()
    }
}

struct TournamentScreen {
    func item(by: Int) -> XCUIElement {
        let scrollViewsQuery = XCUIApplication().scrollViews
        return scrollViewsQuery
            .children(matching: .other)
            .matching(identifier: "TournamentItemView")
            .element(boundBy: by)
    }
}

class ForzaDemoFlowSpec: QuickSpec {
    static func step1Flow() {
        expect(ForzaDemoTestApp.onboarding.nextButton.isEnabled) == false

        ForzaDemoTestApp.onboarding.firstName.tap()
        ForzaDemoTestApp.onboarding.firstName.typeText("First 123 Name")
        expect(ForzaDemoTestApp.onboarding.firstName.text) == "FirstName"

        ForzaDemoTestApp.onboarding.lastName.tap()
        ForzaDemoTestApp.onboarding.lastName.typeText("Last 123 Name")
        expect(ForzaDemoTestApp.onboarding.lastName.text) == "LastName"
        ForzaDemoTestApp.onboarding.lastName.swipeUp()

        ForzaDemoTestApp.onboarding.phone.tap()
        ForzaDemoTestApp.onboarding.phone.typeText("1234dd56ff789")
        expect(ForzaDemoTestApp.onboarding.phone.text) == "(12) 345-67-89"

        sleep(15)
        let app = XCUIApplication()
        ForzaDemoTestApp.onboarding.city.tap()
        app.pickerWheels["Abrikosovka"].tap()
        app.toolbars["Toolbar"].buttons["Done"].tap()

        ForzaDemoTestApp.onboarding.phone.swipeUp()

        ForzaDemoTestApp.onboarding.email.tap()
        ForzaDemoTestApp.onboarding.email.typeText("user@mail.com")
        expect(ForzaDemoTestApp.onboarding.email.text) == "user@mail.com"

        ForzaDemoTestApp.onboarding.password.tap()
        ForzaDemoTestApp.onboarding.password.typeText("securePassword123")
        expect(ForzaDemoTestApp.onboarding.nextButton.isEnabled) == false

        ForzaDemoTestApp.onboarding.confirmPassword.tap()
        ForzaDemoTestApp.onboarding.confirmPassword.typeText("securePassword123")

        expect(ForzaDemoTestApp.onboarding.nextButton.isEnabled)
            .to(beTruthy())
    }
    static func step2Flow() {
        expect(ForzaDemoTestApp.onboarding.registerButton.isEnabled) == false
        ForzaDemoTestApp.onboarding.profile.tap()
        ForzaDemoTestApp.alerts.selectOption(option: "library")
        ForzaDemoTestApp.imagePicker.selectImage(source: .moments, at: 0)
        ForzaDemoTestApp.onboarding.aboutNote.tap()
        ForzaDemoTestApp.onboarding.aboutNote.typeText("About ME")
        expect(ForzaDemoTestApp.onboarding.registerButton.isEnabled)
            .to(beTruthy())
    }

    override func spec() {
        beforeEach {
            XCUIApplication().launch()
        }

        it("Should enter fields and press next") {
            ForzaDemoFlowSpec.step1Flow()
            ForzaDemoTestApp.onboarding.nextButton.tap()
            ForzaDemoFlowSpec.step2Flow()
            ForzaDemoTestApp.onboarding.registerButton.tap()
            ForzaDemoTestApp.tabs.tournamentTab.tap()
            let itemT1T5 = ForzaDemoTestApp.tournamentScreen.item(by: 0)
            sleep(10)
            expect(itemT1T5.text) == "{ T5, T1 }"
        }
    }
}

//
//class RecordTests: XCTestCase {
//    override func setUp() {
//        XCUIApplication().launch()
//        continueAfterFailure = true
//    }
//
//    func testRecord() {
//    }
//}

extension XCUIElement {
    var text: String? {
        return value as? String
    }
}
extension ForzaDemoTestApp {
    static var imagePicker: ImagePicker {
        return ImagePicker()
    }

    static let onboarding: Onboarding = {
        return Onboarding()
    }()
    static let alerts: Alerts  = {
        return Alerts()
    }()
    static let tabs: Tabs  = {
        return Tabs()
    }()
    static let tournamentScreen: TournamentScreen  = {
        return TournamentScreen()
    }()
}

extension XCUIElement {
    func existed() -> Self {
        expect(self.waitForExistence(timeout: 3)) == true
        return self
    }
}
