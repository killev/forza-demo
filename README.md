[![pipeline status](https://gitlab.com/killev/forza-demo/badges/master/pipeline.svg)](https://gitlab.com/killev/forza-demo/commits/master)
[![coverage report](https://gitlab.com/killev/forza-demo/badges/master/coverage.svg)](https://gitlab.com/killev/forza-demo/commits/master)
[![codecov](https://codecov.io/gl/killev/forza-demo/branch/master/graph/badge.svg)](https://codecov.io/gl/killev/forza-demo)

# Professional iOS application development framework

## Content
* Introduction
* [Environment](Documentation/Environment.md)
  * Gitlab
  * [Fastlane](fastlane/README.md)
  * Dependency Management (Carthage)
  * Continious Delivery cycle
    * Test builds
    * Release builds
  * Health checkers and metrics
* [Code](Documentation/Code.md)
  * Code review
  * Static Analyzer
  * Tests
    * Unit tests
    * UI tests
* [Architecture](Documentation/Architecture.md)   
  

## Introduction
> The earlier you find problem - the cheaper the solution

Many companies I'm consulting are thinking I'm a Wizard and can solve their problems immediately and with no effort from their side.

In fact, I can't. 
Sorry!

Any success (and the technical projects also) is a persistent, hard and systematic work. It's rather the process than experience, focus etc.

In my opinion, the prerequisites of technical success are:

### 1. Organization process. I'm not fond of a scrum. I offer a simpler, more flexible yet working process. But:
* grooming
* prioritization
* retrospectives

**must be a part of the whole process**

### 2. Code quality:
* static checkers
* unit tests and behavior tests run with coverage check
* persistent code review

**must be a part of the whole process**

### 3. Product quality:
* manual testing and review by the Product Owner
* health check

**must be a part of the whole process**

### 4. Automatic release cycles 
* all deliveries to all stakeholders should happed right after commint and

**must be a part of the whole process**

Of course, for different technologies all that mean different tools, but items are still the same.

Not everyone agrees on spending effort into automation the process. But those who do - get results in quite a short (2-4 weeks) period of time.

**This framework is a set of concrete tips and recommendation for launching your native iOS application with minimal resurses not sacrafizing the quality.**

