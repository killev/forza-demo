module Fastlane
  module Actions
    class CodecovAction < Action
      def self.run(params)
        cmd = ['curl -s https://codecov.io/bash | bash']

        cmd << "-s --" if params.all_keys.inject(false) { |p, k| p or params[k] }
        cmd << "-X xcodeplist" if params[:use_xcodeplist]
        cmd << "-J '#{params[:project_name]}'" if params[:project_name]
        cmd << "-t '#{params[:token]}'" if params[:token]
        cmd << "-D '#{params[:derived_data_path]}'" if params[:derived_data_path]
        cmd << "-f '#{params[:lcov_path]}'" if params[:lcov_path]
        cmd << "-v" if params[:verbose]
        cmd << "-c" if params[:clean_after]
       

        sh cmd.join(" ")
        sh "rm -f *.coverage.txt"
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "Upload your coverage files to Codecov"
      end

      def self.details
        "https://codecov.io"
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :use_xcodeplist,
                                       env_name: "FL_CODECOV_USE_XCODEPLIST",
                                       description: "[BETA] Upload to Codecov using xcodeplist",
                                       is_string: false,
                                       default_value: false,),
          FastlaneCore::ConfigItem.new(key: :project_name,
                                       env_name: "FL_CODECOV_PROJECT_NAME",
                                       description: "Upload to Codecov using a project name",
                                       optional: true),
          FastlaneCore::ConfigItem.new(key: :token,
                                       env_name: "FL_CODECOV_TOKEN",
                                       description: "API token for private repos",
                                       optional: true),
          FastlaneCore::ConfigItem.new(key: :verbose,
                                       env_name: "FL_CODECOV_VERBOSE",
                                       description: "Verbose logs",
                                       is_string: false,
                                       optional: true,
                                       default_value: false),
          FastlaneCore::ConfigItem.new(key: :clean_after,
                                       #env_name: "FL_CLEAN_AFTER",
                                       description: "Clean after execution",
                                       is_string: false,
                                       optional: true,
                                       default_value: true),
          FastlaneCore::ConfigItem.new(key: :derived_data_path,
                                       env_name: "FL_DERIVED_DATA",
                                       description: "Path to derived data",
                                       is_string: true,
                                       optional: true),
          FastlaneCore::ConfigItem.new(key: :lcov_path,
                                       env_name: "FL_LCOV",
                                       description: "Path to lcov file",
                                       is_string: true,
                                       optional: true)
        ]
      end

      def self.author
        "Peter Ovchinnikov (@killev)"
      end

      def self.is_supported?(platform)
        true
      end
    end
  end
end