//
//  City.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/3/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Alamofire

//{
//   "country": "AD",
//   "name": "les Escaldes",
//   "lat": "42.50729",
//   "lng": "1.53414"
// },

struct City {
    let country: String
    let name: String //"\u0410\u0433\u0434\u0430\u043c",
    let lat: String // 39.991094,
    let lng: String // 46.9297085
}

extension City: Decodable {
    enum CodingKeys: String, CodingKey {
        case country
        case name
        case lat
        case lng
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        name = try values.decode(String.self, forKey: .name)
        lat = try values.decode(String.self, forKey: .lat)
        lng = try values.decode(String.self, forKey: .lng)
        country = try values.decode(String.self, forKey: .country)
    }
}
