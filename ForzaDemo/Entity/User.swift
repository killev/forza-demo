//
//  User.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 8/30/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import UIKit

struct User {
    let firstName: String
    let lastName: String
    let city: String
    let email: String
    let password: String
    let note: String
    let image: UIImage?
}

extension User {

    func patch(firstName: String? = nil,
               lastName: String? = nil,
               city: String? = nil,
               email: String? = nil,
               password: String? = nil,
               note: String? = nil,
               image: UIImage? = nil) -> User {
        return User(firstName: firstName ?? self.firstName,
                    lastName: lastName ?? self.lastName,
                    city: city ?? self.city,
                    email: email ?? self.email,
                    password: password ?? self.password,
                    note: note ?? self.note,
                    image: image ?? self.image)
    }
}
