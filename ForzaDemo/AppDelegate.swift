//
//  AppDelegate.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 8/30/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import AppCenter
//import AppCenterAnalytics
import AppCenterCrashes
import ReaktiveBoar
import IQKeyboardManagerSwift

//this is a simple line for gitlab article

//another one simple line for gitlab article

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        #if targetEnvironment(simulator)
        // Disable hardware keyboards.
        let setHardwareLayout = NSSelectorFromString("setHardwareLayout:")
        UITextInputMode.activeInputModes
            // Filter `UIKeyboardInputMode`s.
            .filter({ $0.responds(to: setHardwareLayout) })
            .forEach { $0.perform(setHardwareLayout, with: nil) }
        #endif

        guard !UIApplication.isTesting else {

            let window = UIWindow(frame: UIScreen.main.bounds)
            window.makeKeyAndVisible()
            window.rootViewController = UIViewController()
            self.window = window

            return true
        }

        MSAppCenter.start("664bf0b4-ed26-4cde-8ff4-afd13d0de501", withServices: [
          MSCrashes.self
        ])

        IQKeyboardManager.shared.enable = true
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        window.makeKeyAndVisible()
        ReaktiveBoar.setup()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
}

extension UIApplication {
    static var isTesting: Bool {
        #if DEBUG
        return ProcessInfo.processInfo
            .environment["XCTestConfigurationFilePath"] != nil
        #else
        return false
        #endif
    }
}
