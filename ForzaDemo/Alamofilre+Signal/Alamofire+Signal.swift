//swiftlint:disable:this file_name
//
//  Alamofire+Signal.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 25.10.2019.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReactiveKit
import Alamofire

public typealias ResponseSignal<T> = Signal<T, NSError>

public extension DataRequest {

    func responseObject<T: Decodable>(_ type: T.Type,
                                      queue: DispatchQueue = .main,
                                      keyPath: String? = nil,
                                      context: Any? = nil) -> ResponseSignal<T> {

        return ResponseSignal { observer in

            self.responseJSON(queue: queue,
                              options: .allowFragments) { response in

                                switch response.result {

                                case .success:
                                    do {
                                        let object = try JSONDecoder().decode(type, from: response.data!)
                                        observer.receive(lastElement: object)
                                    } catch {
                                        print(error)
                                        observer.receive(completion: .failure(error as NSError))
                                    }
                                case .failure(let error):
                                    observer.receive(completion: .failure(error as NSError))
                                }
            }
            return NonDisposable.instance
        }
    }

    func responseArray<T: Decodable>(_ type: T.Type,
                                     queue: DispatchQueue = .main,
                                     keyPath: String? = nil,
                                     context: Any? = nil) -> ResponseSignal<[T]> {

        return ResponseSignal { observer in

            self.responseJSON(queue: queue,
                              options: .allowFragments) { response in

                                switch response.result {

                                case .success:
                                    do {
                                        let object = try JSONDecoder().decode([T].self, from: response.data!)
                                        observer.receive(lastElement: object)
                                    } catch {
                                        observer.receive(completion: .failure(error as NSError))
                                    }
                                case .failure(let error):
                                    observer.receive(completion: .failure(error as NSError))
                                }
            }
            return NonDisposable.instance
        }
    }
}
