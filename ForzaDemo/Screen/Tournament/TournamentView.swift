//
//  TournamentView.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/10/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//
import Foundation
import UIKit

class TournamentItemView: UIView {
    let topLabel = UILabel()
    let bottomLabel = UILabel()

    init(frame: CGRect, item: TournamentItem) {
        super.init(frame: frame)
        addSubview(topLabel)
        addSubview(bottomLabel)
        topLabel.snp.makeConstraints { maker in
            maker.left.right.top.equalTo(self)
            maker.height.equalTo(self.snp.height).dividedBy(2)
        }
        bottomLabel.snp.makeConstraints { maker in
            maker.left.right.bottom.equalTo(self)
            maker.height.equalTo(self.snp.height).dividedBy(2)
        }
        topLabel.text = item.team1
        bottomLabel.text = item.team2
        layer.borderWidth = 2
        layer.borderColor = UIColor.gray.cgColor
        layer.cornerRadius = 5
        clipsToBounds = true
        isAccessibilityElement = true
        accessibilityIdentifier = "TournamentItemView"
        accessibilityValue = "{ \(item.team1), \(item.team2) }"
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class TournamentView: UIScrollView, UIScrollViewDelegate {

    let builder = TreeViewBuilder<TournamentItem, UIView>()
    let width = 150
    let height = 50
    let connectorWidth = 75

    func createView(ver: Int, level: Int, item: TournamentItem) -> [UIView] {

        let origin = CGPoint(x: level * (self.width + self.connectorWidth), y: ver * self.height)
        let size = CGSize(width: self.width, height: self.height)
        let frame = CGRect(origin: origin, size: size)

        let view = TournamentItemView(frame: frame, item: item)
        //view.backgroundColor = .blue
        return [view]
    }
    func createConnections(vertFrom: Int, vertTo: Int, level: Int) -> [UIView] {

        let res = (vertFrom...vertTo).map { index -> TournamentConnectorView in
            let origin = CGPoint(x: level * (self.width + self.connectorWidth) + self.width, y: index * self.height)
            let size = CGSize(width: self.connectorWidth, height: self.height)
            let frame = CGRect(origin: origin, size: size)

            switch index {
            case vertFrom: return TournamentConnectorView(frame: frame, type: .top)
            case vertTo: return TournamentConnectorView(frame: frame, type: .bottom)
            case (vertFrom + vertTo) / 2: return TournamentConnectorView(frame: frame, type: .result)
            default: return TournamentConnectorView(frame: frame, type: .vertical)
            }
        }

        res.forEach {
            $0.foregroundColor = .gray
            $0.backgroundColor = self.backgroundColor
        }
        return res
    }

    var tree: BITreeNode<TournamentItem>? = nil {
        didSet {
            createTournamentNet()
        }
    }

    func createTournamentNet() {
        self.subviews.forEach { $0.removeFromSuperview() }
        guard let tree = tree else {
            return
        }
        builder.process(node: tree,
                        createView: self.createView,
                        createConnections: self.createConnections)
            .forEach(self.addSubview)

        let maxX = self.subviews.map { $0.frame.maxX }.max() ?? 0
        let maxY = self.subviews.map { $0.frame.maxY }.max() ?? 0
        self.contentSize = CGSize(width: maxX, height: maxY)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
