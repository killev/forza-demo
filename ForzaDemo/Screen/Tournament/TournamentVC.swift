//
//  TournamentVC.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/1/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit
import Bond

struct TournamentItem {
    let team1: String
    let team2: String
}

class TournamentVC: UIViewController {
    @IBOutlet var tournamentView: TournamentView!
}

extension TournamentVC: VCView {
    typealias VMType = TournamentVM
    func advise(vm: TournamentVM, bag: DisposeBag) {
        tournamentView.tree = SampleTree.tree
    }
}
