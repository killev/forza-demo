//
//  TournamentConnectorView.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/10/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import UIKit

enum ConnectorType {
    case top
    case bottom
    case vertical
    case result
}

class TournamentConnectorView: UIView {
    let type: ConnectorType
    var foregroundColor: UIColor = .red
    init(frame: CGRect, type: ConnectorType) {
        self.type = type
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func draw(_ rect: CGRect) {

        backgroundColor?.setFill()
        UIGraphicsGetCurrentContext()!.fill(rect)
        let path = UIBezierPath()
        switch type {

        case .top:
            path.move(to: CGPoint(x: rect.minX, y: rect.midY))
            path.addLine(to: CGPoint(x: rect.midX, y: rect.midY))
            path.addLine(to: CGPoint(x: rect.midX, y: rect.maxY))
        case .bottom:
            path.move(to: CGPoint(x: rect.minX, y: rect.midY))
            path.addLine(to: CGPoint(x: rect.midX, y: rect.midY))
            path.addLine(to: CGPoint(x: rect.midX, y: rect.minY))
        case .vertical:
            path.move(to: CGPoint(x: rect.midX, y: rect.minY))
            path.addLine(to: CGPoint(x: rect.midX, y: rect.maxY))
        case .result:
            path.move(to: CGPoint(x: rect.midX, y: rect.minY))
            path.addLine(to: CGPoint(x: rect.midX, y: rect.maxY))
            path.move(to: CGPoint(x: rect.maxX, y: rect.midY))
            path.addLine(to: CGPoint(x: rect.midX, y: rect.midY))
        }

        foregroundColor.set()
        path.stroke()
    }
}
