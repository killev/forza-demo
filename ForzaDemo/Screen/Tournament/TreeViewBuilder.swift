//
//  TreeViewBuilder.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/10/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Foundation
import UIKit

class TreeViewBuilder<Item, T> {
    typealias CreateView = (Int, Int, Item) -> [T]
    typealias CreateConnectorView = (Int, Int, Int) -> [T]
    init() { }

    func process(node: BITreeNode<Item>,
                 createView: CreateView,
                 createConnections: CreateConnectorView) -> [T] {
        let level = node.depth - 1
        return processNode(node: node,
                           pos: pow(2, level) - 1,
                           level: level,
                           createView: createView,
                           createConnections: createConnections)
    }
    private func processNode(node: BITreeNode<Item>?,
                             pos: Int,
                             level: Int,
                             createView: CreateView,
                             createConnections: CreateConnectorView) -> [T] {

        guard let node = node else {
            return []
        }
        var res = createView(pos, level, node.value)

        if node.hasChildren {
            let relativeDepth = pow(2, level - 1)
            res += createConnections(pos - relativeDepth,
                                     pos + relativeDepth,
                                     level - 1)

            res += processNode(node: node.left,
                               pos: pos - relativeDepth,
                               level: level - 1,
                               createView: createView,
                               createConnections: createConnections)

            res += processNode(node: node.right,
                               pos: pos + relativeDepth,
                               level: level - 1,
                               createView: createView,
                               createConnections: createConnections)
        }

        return res
    }
}
