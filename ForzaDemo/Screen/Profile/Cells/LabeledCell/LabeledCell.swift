//
//  LabeledCell.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/6/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

class LabeledCell: UICollectionViewCell {
    @IBOutlet var name: UILabel!
    @IBOutlet var label: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.translatesAutoresizingMaskIntoConstraints = false
    }

    override func systemLayoutSizeFitting(_ targetSize: CGSize,
                                          withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority,
                                          verticalFittingPriority: UILayoutPriority) -> CGSize {

        let frame = UIApplication.shared.keyWindow!.frame
        contentView.snp.remakeConstraints { maker in
            maker.width.equalTo(frame.width)
        }
        return contentView.systemLayoutSizeFitting(CGSize(width: frame.width, height: 1))
    }
}

extension LabeledCell: Cell {
    typealias VMType = LabeledCellVM
    func advise(vm: LabeledCellVM, bag: DisposeBag) {

        vm.name
            .bind(to: name)
            .dispose(in: bag)

        vm.label
            .bind(to: label)
            .dispose(in: bag)
    }
}
