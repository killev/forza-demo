//
//  LabeledCellVM.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/6/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

class LabeledCellVM: VM {
    let name: SafeSignal<String>
    let label: SafeSignal<String>
    init(name: SafeSignal<String>, label: SafeSignal<String>) {
        self.label = label
        self.name = name
        super.init()
    }
}
