//
//  ImageCellVM.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/6/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

class ImageCellVM: VM {
    let image: SafeSignal<UIImage>
    init(image: SafeSignal<UIImage>) {
        self.image = image
        super.init()
    }
}
