//
//  ImageCell.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/6/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit
import SnapKit

class ImageCell: UICollectionViewCell {
    @IBOutlet var image: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.translatesAutoresizingMaskIntoConstraints = false
    }

    override func systemLayoutSizeFitting(_ targetSize: CGSize,
                                          withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority,
                                          verticalFittingPriority: UILayoutPriority) -> CGSize {
        let frame = UIApplication.shared.keyWindow!.frame
        contentView.snp.remakeConstraints { maker in
            maker.width.equalTo(frame.width)
            maker.height.equalTo(250)
        }

        return contentView.systemLayoutSizeFitting(CGSize(width: frame.width, height: 250))
    }
}

extension ImageCell: Cell {
    typealias VMType = ImageCellVM
    func advise(vm: ImageCellVM, bag: DisposeBag) {

        vm.image
            .bind(to: image)
            .dispose(in: bag)
    }
}
