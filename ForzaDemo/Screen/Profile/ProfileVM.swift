//
//  ProfileVM.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 8/31/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import Swinject
import SwinjectAutoregistration
import ReactiveKit

class ProfileVM: VCVM {
    let dataSource: Property<[VM]>
    required init(resolver: Resolver) {
        let user: User = resolver~>

        let image = ImageCellVM(image: user.image!.s())
        let firstName = LabeledCellVM(name: "First Name:".s(), label: user.firstName.s())
        let lastName = LabeledCellVM(name: "Last Name:".s(), label: user.lastName.s())
        let email = LabeledCellVM(name: "Email:".s(), label: user.email.s())
        let city = LabeledCellVM(name: "City:".s(), label: user.city.s())

        let about = LabeledCellVM(name: "About:".s(), label: user.note.s())

        dataSource = Property([image,
                               firstName,
                               lastName,
                               email,
                               city,
                               about
            ])
        super.init(resolver: resolver)
    }
}
