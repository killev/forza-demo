//
//  ProfileVC.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 8/31/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

class ProfileVC: UIViewController {
    @IBOutlet var collectionView: UICollectionView!
}

extension ProfileVC: VCView {
    typealias VMType = ProfileVM

    func advise(vm: ProfileVM, bag: DisposeBag) {
        vm.dataSource.bind(to: collectionView,
                           estimatedItemSize: CGSize(width: 50, height: 50),
                           with: self.resolver) { register in
                            register.register(register: LabeledCell.self)
                            register.register(register: ImageCell.self)
        }.dispose(in: bag)
    }
}
