//
//  Onboarding2VM.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 8/31/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Swinject
import SwinjectAutoregistration
import ReaktiveBoar
import ReactiveKit

class Onboarding2VM: VCVM {

    let register = PassthroughSubject<Void, Never>()
    let dataSource: Property<[VM]>
    let valid: SafeSignal<Bool>

    required init(resolver: Resolver) {
        let profileImage = ProfileImageCellVM(router: resolver~>)
        let note = NoteCellVM()

        dataSource = Property([
            profileImage,
            note])
        let userSerivice: Property<User?> = resolver~>
        let user: User = resolver~>

        valid = combineLatest(profileImage.valid,
                              note.valid)
            .map { $0 && $1 }

        super.init(resolver: resolver)
        register
            .with(.strong(user), .val(note.text), .val(profileImage.image))
            .map { $0.patch(note: $1, image: $2) }
            .bind(to: userSerivice)
            .dispose(in: bag)
    }
}
