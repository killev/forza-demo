//
//  Onboarding2VC.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 8/31/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

class Onboarding2VC: UIViewController {
    @IBOutlet var register: UIButton!
    @IBOutlet var collectionView: UICollectionView!

    @IBOutlet var bottom: NSLayoutConstraint!
}

extension Onboarding2VC: VCView {
    func advise(vm: Onboarding2VM, bag: DisposeBag) {

        vm.dataSource.bind(to: collectionView, with: resolver) { register in
            register.register(register: ProfileImageCell.self,
                              with: SizeInfo(width: .full(offset: 0),
                                             height: .const(200)))
            register.register(register: NoteCell.self,
                              with: SizeInfo(width: .full(offset: 0),
                                             height: .const( 450)))
        }.dispose(in: bag)

        register.reactive.tap
            .bind(to: vm.register)
            .dispose(in: bag)

        vm.valid
            .bind(to: register.reactive.isEnabled)
            .dispose(in: bag)

        NotificationCenter.default.reactive.keyboardHeight
            .map { $0 + 20 }
            .bind(to: bottom.reactive.constant(weak: view) { view in
                    UIView.animate(withDuration: 0.3) { view.layoutIfNeeded() }
            }).dispose(in: bag)

        view.reactive.tapGesture()
            .eraseType()
            .bind(to: view.reactive.endEditing(true))
            .dispose(in: bag)
    }

    typealias VMType = Onboarding2VM
}
