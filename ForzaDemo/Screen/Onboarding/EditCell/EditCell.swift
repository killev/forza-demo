//
//  EditCell.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/1/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

import AnyFormatKit

typealias CharacterValidator = (Character) -> Bool

class TextInputController: NSObject, UITextFieldDelegate {

    var formatter: TextInputFormatter?

    var charValidator: CharacterValidator = {_ in true }

    init(formatter: TextInputFormatter?) {
        self.formatter = formatter
    }

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {

        let valid = string.allSatisfy(charValidator)

        guard valid else {
            return false
        }

        guard let formatter = formatter else {
            return true
        }

        print(textField.text ?? "")
        print(range)
        print(string)

        let result = formatter.formatInput(currentText: textField.text ?? "", range: range, replacementString: string)
        textField.text = result.formattedText
        textField.sendActions(for: .editingChanged)
        textField.setCursorLocation(result.caretBeginOffset)

        return false
    }
}

class InsetTextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.delegate = controller
    }

    var formatter: TextInputFormatter? {
        set {
            controller.formatter = newValue
        }
        get {
            return controller.formatter
        }
    }

    var charValidator: CharacterValidator {
        get {
            return controller.charValidator
        }
        set {
            controller.charValidator = newValue
        }
    }

    private let controller = TextInputController(formatter: nil)

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = controller
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class EditCell: UICollectionViewCell {
    @IBOutlet var edit: InsetTextField!
    @IBOutlet var prefix: UILabel!
    @IBOutlet var surrounding: UIView!
}

extension EditCell: Cell {
    func advise(vm: EditCellVM, bag: DisposeBag) {
        edit.placeholder = vm.initText
        prefix.text = vm.prefix

        edit.keyboardType = vm.keyboardType
        edit.formatter = vm.formatter
        edit.charValidator = vm.charValidator

        edit.isSecureTextEntry = vm.isSecureTextEntry

        vm.valid
            .map { $0 ? UIColor.white : UIColor.red }
            .bind(to: surrounding.reactive.borderColor)
            .dispose(in: bag)

        let formatter = vm.formatter
        vm.text.bidirectionalMap(to: { formatter?.format($0) ?? $0 }, from: { txt in
            let res = formatter?.unformat(txt) ?? txt ?? ""
            return res
        })
            .bidirectionalBind(to: edit.reactive.text)
            .dispose(in: bag)
    }

    typealias VMType = EditCellVM
}

extension DefaultTextInputFormatter {
    static let phoneNumber = DefaultTextInputFormatter(textPattern: "(##) ###-##-##")
}
private extension UITextField {

    func setCursorLocation(_ location: Int) {
        if let cursorLocation = position(from: beginningOfDocument, offset: location) {
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.selectedTextRange = strongSelf.textRange(from: cursorLocation, to: cursorLocation)
            }
        }
    }
}
