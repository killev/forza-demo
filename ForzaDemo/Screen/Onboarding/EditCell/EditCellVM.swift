//
//  EditCellVM.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/1/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit
import AnyFormatKit

class EditCellVM: VM {
    let text: Property<String>
    let valid: SafeSignal<Bool>

    let initText: String
    let prefix: String
    let formatter: DefaultTextInputFormatter?
    let keyboardType: UIKeyboardType
    let charValidator: CharacterValidator
    let isSecureTextEntry: Bool

    init(text: Property<String>,
         valid: SafeSignal<Bool>,
         prefix: String,
         initText: String,
         formatter: DefaultTextInputFormatter?,
         keyboardType: UIKeyboardType,
         isSecureTextEntry: Bool,
         charValidator: @escaping CharacterValidator) {

        self.text = text
        self.valid = valid

        self.prefix = prefix
        self.initText = initText
        self.formatter = formatter
        self.keyboardType = keyboardType
        self.charValidator = charValidator
        self.isSecureTextEntry = isSecureTextEntry

        super.init()
    }
}

extension EditCellVM {
    convenience init(prefix: String,
                     initText: String,
                     formatter: DefaultTextInputFormatter?,
                     keyboardType: UIKeyboardType,
                     isSecureTextEntry: Bool,
                     charValidator: @escaping CharacterValidator,
                     validator: @escaping (String) -> Bool) {

        let text = Property("")
        let valid = text.map(validator)
        self.init(text: text,
                  valid: valid,
                  prefix: prefix,
                  initText: initText,
                  formatter: formatter,
                  keyboardType: keyboardType,
                  isSecureTextEntry: isSecureTextEntry,
                  charValidator: charValidator)
    }
}
