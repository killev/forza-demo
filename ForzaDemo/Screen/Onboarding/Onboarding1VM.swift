//
//  OnboardingVM.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 8/30/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import Swinject
import SwinjectAutoregistration
import ReactiveKit
import Bond

class Onboarding1VM: VCVM {
    let next = PassthroughSubject<Void, Never>()
    let dataSource: Property<[VM]>
    let valid: SafeSignal<Bool>

    //swiftlint:disable:next function_body_length
    required init(resolver: Resolver) {

        let firstName = EditCellVM(prefix: "",
                                   initText: "First Name",
                                   formatter: nil,
                                   keyboardType: .namePhonePad,
                                   isSecureTextEntry: false,
                                   charValidator: { $0.isLetter },
                                   validator: Validator.nameValidator)

        let lastName = EditCellVM(prefix: "",
                                  initText: "Last Name",
                                  formatter: nil,
                                  keyboardType: .namePhonePad,
                                  isSecureTextEntry: false,
                                  charValidator: { $0.isLetter },
                                  validator: Validator.nameValidator)

        let phone = EditCellVM(prefix: "+380",
                               initText: "(12) 345-67-89",
                               formatter: .phoneNumber,
                               keyboardType: .phonePad,
                               isSecureTextEntry: false,
                               charValidator: { $0.isNumber },
                               validator: Validator.phoneValidator)

        let city = CityCellVM(router: resolver~>)

//        let city = EditCellVM(prefix: "",
//                              initText: "City",
//                              formatter: nil,
//                              keyboardType: .namePhonePad,
//                              isSecureTextEntry: false,
//                              charValidator: { $0.isLetter },
//                              validator: Validator.nameValidator)

        let email = EditCellVM(prefix: "",
                               initText: "user@email.com",
                               formatter: nil,
                               keyboardType: .emailAddress,
                               isSecureTextEntry: false,
                               charValidator: { _ in true },
                               validator: Validator.emailValidator)

        let password = EditCellVM(prefix: "",
                                  initText: "Password",
                                  formatter: nil,
                                  keyboardType: .default,
                                  isSecureTextEntry: true,
                                  charValidator: { _ in true },
                                  validator: Validator.passwordValidator)

        let confirmPasswordText = Property<String>("")
        let confirmValid = combineLatest(password.valid, password.text, confirmPasswordText)
            .map { $0 && $1 == $2 }

        let confirmPassword = EditCellVM(text: confirmPasswordText,
                                         valid: confirmValid,
                                         prefix: "",
                                         initText: "Confirm password",
                                         formatter: nil,
                                         keyboardType: .default,
                                         isSecureTextEntry: true,
                                         charValidator: { _ in true })
        dataSource = Property([
            firstName,
            lastName,
            phone,
            email,
            city,
            password,
            confirmPassword
            ])

        valid = combineLatest(firstName.valid,
                              lastName.valid,
                              phone.valid,
                              email.valid,
                              city.valid,
                              password.valid)
            .map { $0 && $1 && $2 && $3 && $4 && $5 }
            .combineLatest(with: confirmPassword.valid)
            .map { $0 && $1 }

        super.init(resolver: resolver)
        let router: RouterProtocol1 = resolver~>

        next.with(.val(firstName.text),
                  .val(lastName.text),
                  .val(city.city),
                  .val(email.text),
                  .val(password.text))
            .map { user in
                User(firstName: user.0,
                     lastName: user.1,
                     city: user.2,
                     email: user.3,
                     password: user.4,
                     note: "",
                     image: nil)
            }.bind(to: router.show(Onboarding2VC.VMType.self))
            .dispose(in: bag)
    }
}
