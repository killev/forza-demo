//
//  NoteCellVM.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/4/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

class NoteCellVM: VM {
    let text = Property<String>("")
    let valid: SafeSignal<Bool>
    override init() {
        valid = text.map { !$0.isEmpty }
        super.init()
    }
}
