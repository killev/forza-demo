//
//  NoteCell.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/4/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

class NoteCell: UICollectionViewCell {
    @IBOutlet var note: UITextView!
    @IBOutlet var surrounding: UIView!
}

extension NoteCell: Cell {
    func advise(vm: NoteCellVM, bag: DisposeBag) {

        vm.text
            .bidirectionalMap(to: { $0 }, from: { $0 ?? "" })
            .bidirectionalBind(to: note.reactive.text)
            .dispose(in: bag)

        vm.valid
            .map { $0 ? UIColor.white : UIColor.red }
            .bind(to: surrounding.reactive.borderColor)
            .dispose(in: bag)
    }

    typealias VMType = NoteCellVM
}
