//
//  ProfileImageCell.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/3/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

class ProfileImageCell: UICollectionViewCell {
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var surrounding: UIView!
}

extension ProfileImageCell: Cell {

    func advise(vm: ProfileImageCellVM, bag: DisposeBag) {
        vm.image
            .recoverNils(UIImage(named: "profile-no-image")!)
            .bind(to: profileImage)
            .dispose(in: bag)

        reactive.didSelect
            .bind(to: vm.didSelect)
            .dispose(in: bag)

        vm.valid
            .map { $0 ? UIColor.white : UIColor.red }
            .bind(to: surrounding.reactive.borderColor)
            .dispose(in: bag)
    }

    typealias VMType = ProfileImageCellVM
}
