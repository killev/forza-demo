//
//  ProfileImageCellVM.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/3/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

enum ImagePickerStyle: ActionType {
    case library
    case camera
    case cancel
}

class ProfileImageCellVM: VM {
    let image = Property<UIImage?>(nil)
    let didSelect = PassthroughSubject<Void, Never>()
    let valid: SafeSignal<Bool>
    init(router: RouterProtocol1) {

        valid = image.map { $0 != nil }
        super.init()

        let imageSignal = didSelect.flatMapLatest {
            router.showActions(ImagePickerStyle.self, title: "Select", message: "Message")
        }.map { option -> UIImagePickerController? in
                let picker = UIImagePickerController()
                switch option {
                case .camera: picker.sourceType = .camera
                case .library: picker.sourceType = .photoLibrary
                default: return nil
                }
                return picker
        }.ignoreNils()
        .flatMapLatest(router.showPicker)
        .safe(error: router.showError(title: "", msg: { $0.localizedDescription }))
        .share()
        //
        imageSignal.eraseType().bind(to: router.closeAll()).dispose(in: bag)
        imageSignal
            .map { $0.image }
            .bind(to: image).dispose(in: bag)
    }
}

extension ImagePickerResult {
    var image: UIImage? {
        switch self {
        case .image(let image): return image
        default: return nil
        }
    }
}

extension ImagePickerStyle {

    var title: String {
        return "\(self)"
    }

    var style: UIAlertAction.Style {
        switch self {
        case .cancel:
            return .cancel
        default:
            return .default
        }
    }
}
