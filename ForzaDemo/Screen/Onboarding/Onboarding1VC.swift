//
//  Onboarding1VC.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 8/30/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit
import Bond

class Onboarding1VC: UIViewController {
    @IBOutlet var nextScreen: UIButton!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var bottom: NSLayoutConstraint!
}

extension Onboarding1VC: VCView {
    func advise(vm: Onboarding1VM, bag: DisposeBag) {
        vm.dataSource
            .bind(to: collectionView, with: self.resolver) { register in
                register.register(register: EditCell.self,
                                  with: SizeInfo(width: .full(offset: 0), height: .const(40)))
                register.register(register: CityCell.self,
                                  with: SizeInfo(width: .full(offset: 0), height: .const(40)))
            }.dispose(in: bag)

        nextScreen.reactive.tap
            .bind(to: vm.next)
            .dispose(in: bag)

        vm.valid
            .bind(to: nextScreen.reactive.isEnabled)
            .dispose(in: bag)

        NotificationCenter.default.reactive.keyboardHeight
            .map { $0 + 20 }
            .bind(to: bottom.reactive
                .constant(weak: view) { view in
                    UIView.animate(withDuration: 0.3) { view.layoutIfNeeded() }
                }).dispose(in: bag)

        view.reactive.tapGesture()
            .eraseType()
            .bind(to: view.reactive.endEditing(true))
            .dispose(in: bag)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.reloadData()
    }

    typealias VMType = Onboarding1VM
}
