//
//  CityCell.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/3/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit
import Bond
import CoreActionSheetPicker

class CityCell: UICollectionViewCell {
    @IBOutlet var city: UILabel!
    @IBOutlet var surrounding: UIView!
    @IBOutlet var selectButton: UIButton!
}

extension CityCell: Cell {
    typealias VMType = CityCellVM
    func advise(vm: CityCellVM, bag: DisposeBag) {

       selectButton.reactive.tap
            .with(.strong(vm.cities))
            .flatMapLatest { $0.ignoreNils() }
            .with(.val(vm.city))
            .flatMapLatest(ActionSheetStringPicker.reactive.show(title: "Select City"))
            .map { $0.value }
            .ignoreNils()
            .bind(to: vm.city)
            .dispose(in: bag)

        vm.valid
            .map { $0 ? UIColor.white : UIColor.red }
            .bind(to: surrounding.reactive.borderColor)
            .dispose(in: bag)

        vm.city
            .bind(to: city)
            .dispose(in: bag)
    }
}
