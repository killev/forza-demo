//
//  CityCellVM.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/3/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit
import Bond
import Alamofire

class CityCellVM: VM {
    let city = Property<String>("")
    let cities = Property<[String]?>(nil)
    let valid: SafeSignal<Bool>

    private let errors = PassthroughSubject<NSError, Never>()
    init(router: RouterProtocol1) {

        valid = city.map { !$0.isEmpty }
        super.init()
        Alamofire.request("https://raw.githubusercontent.com/lutangar/cities.json/master/cities.json")
            .responseArray(City.self)
            .suppressError(logging: true)
            .map { $0.filter { $0.country == "UA" }.map { $0.name }.sorted() }
            .bind(to: cities)
            .dispose(in: bag)

        errors.bind(to: router.showError(title: "Error",
                                         msg: { $0.localizedDescription }))
    }
}
