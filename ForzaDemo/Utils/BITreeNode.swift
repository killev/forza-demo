//
//  BITreeNode.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/10/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Foundation

class BITreeNode<T> {
    let value: T
    let left: BITreeNode<T>?
    let right: BITreeNode<T>?
    init(_ value: T) {
        self.value = value
        left = nil
        right = nil
    }
    init(_ value: T, left: BITreeNode<T>, right: BITreeNode<T>) {
        self.value = value
        self.left = left
        self.right = right
    }
}

extension BITreeNode {
    var depth: Int {
        let leftDepth = left?.depth ?? 0
        let rightDepth = right?.depth ?? 0
        return max(leftDepth, rightDepth) + 1
    }
    var hasChildren: Bool {
       return left != nil || right != nil
    }
}
