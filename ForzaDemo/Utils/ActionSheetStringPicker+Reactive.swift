//
//  ActionSheetStringPicker+Reactive.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/3/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReactiveKit
import CoreActionSheetPicker

struct ActionSheetStringPickerReactive { }

enum ActionSheetResult<T> {
    case done(index: Int, value: T?)
    case cancel
}

extension ActionSheetResult {
    var value: T? {
        switch self {
        case .done(_, let value): return value
        default: return nil
        }
    }
}
extension ActionSheetStringPickerReactive {
    func show(title: String,
              items: [String],
              current: String) -> SafeSignal<ActionSheetResult<String>> {

        return SafeSignal { observer in
            ExecutionContext.immediateOnMain.execute {
                let initialSelection = items.firstIndex(of: current) ?? 0
                ActionSheetStringPicker.show(
                    withTitle: title,
                    rows: items,
                    initialSelection: initialSelection,
                    doneBlock: { _, index, value in
                        observer.receive(lastElement: .done(index: index, value: value as? String))
                    }, cancel: { _ in
                        observer.receive(lastElement: .cancel)
                    },
                   origin: nil)
            }
            return observer.disposable
        }
    }

    func show(title: String) -> ([String], String) -> SafeSignal<ActionSheetResult<String>> {
        return { self.show(title: title, items: $0, current: $1) }
    }
}

extension ActionSheetStringPicker {
    static let reactive = ActionSheetStringPickerReactive()
}
