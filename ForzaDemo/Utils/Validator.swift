//
//  Validator.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/2/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Foundation

struct Validator {
}

extension Validator {
    static func nameValidator(text: String) -> Bool {
        return text.count > 3 && text.allSatisfy { $0.isLetter }
    }

    static func phoneValidator(text: String) -> Bool {
        return text.count == 9 && text.allSatisfy { $0.isNumber }
    }

    static func emailValidator(text: String) -> Bool {

        // swiftlint:disable:next line_length
        let patterm = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"

        // swiftlint:disable:next force_try
        let regex = try! NSRegularExpression(pattern: patterm,
                                             options: .caseInsensitive)
        return regex.firstMatch(in: text, options: [], range: NSRange(location: 0, length: text.count)) != nil
    }

    static func confirmValidation(current: @escaping () -> String) -> (String) -> Bool {
        return {text in current() == text }
    }

    static func passwordValidator(text: String) -> Bool {
        return text.count >= 6 //&& text.allSatisfy { !$0.isCyrillic }
    }
}

extension Character {
    var isCyrillic: Bool {
        let upper = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЮЯ"
        let lower = "абвгдежзийклмнопрстуфхцчшщьюя"

        return upper.contains(self) || lower.contains(self)
    }
}
