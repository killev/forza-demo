//
//  SampleTree.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/9/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Bond

struct SampleTree {
    static let final = TournamentItem(team1: "T1",
                                      team2: "T2")

    static let semiFinal1 = TournamentItem(team1: "T3",
                                           team2: "T1") // win

    static let semiFinal2 = TournamentItem(team1: "T4",
                                           team2: "T2") // win

    static let quoterFinal1 = TournamentItem(team1: "T5",
                                             team2: "T1") // win

    static let quoterFinal2 = TournamentItem(team1: "T6",
                                             team2: "T2") // win

    static let quoterFinal3 = TournamentItem(team1: "T7",
                                             team2: "T3") // win

    static let quoterFinal4 = TournamentItem(team1: "T8",
                                             team2: "T4") //win

    static let tree = BITreeNode(final,
                                 left: BITreeNode(semiFinal1,
                                                  left: BITreeNode(quoterFinal1),
                                                  right: BITreeNode(quoterFinal2)),

                                 right: BITreeNode(semiFinal2,
                                                   left: BITreeNode(quoterFinal3),
                                                   right: BITreeNode(quoterFinal4)))
}
