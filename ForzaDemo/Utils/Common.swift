//
//  Common.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 9/10/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//
import Foundation

func pow(_ base: Int, _ deg: Int) -> Int {
    return Int(Foundation.pow( Double(base), Double(deg)))
}
