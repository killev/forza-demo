//swiftlint:disable:this file_name
//
//  AppContext.swift
//  ForzaDemo
//
//  Created by Peter Ovchinnikov on 8/31/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import ReaktiveBoar
import Swinject
import SwinjectAutoregistration
import ReactiveKit

extension UIApplication: Assembly {
    // Register router and other global services here

    public func assemble(container: Container) {
        container.register(RouterProtocol1.self) { resolver in
            return Router1(parent: resolver)
        }.initCompleted { _, router in
            router.register(Onboarding1VC.self)
            router.register(Onboarding2VC.self)
        }.inObjectScope(.container)
        container.register(value: Property<User?>(nil)).inObjectScope(.container)
    }

    // Implement global app logic and transitions
    public func loaded(resolver: Resolver) {
        let userService: Property<User?> = resolver~>
        let router: RouterProtocol1 = resolver~>

//        let user = User(firstName: "First Name",
//                        lastName: "Last Name",
//                        city: "London",
//                        email: "email@delete.com",
//                        password: "password",
//                        //swiftlint:disable:next line_length
//                        note: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis eget arcu ac bibendum. Morbi fringilla auctor finibus. Aenean consectetur imperdiet orci, ullamcorper faucibus dui lobortis eget. Cras sed mauris massa. Nunc accumsan tortor a enim hendrerit lobortis. Sed fringilla libero magna. Nunc vel dolor eros. Cras volutpat libero quis malesuada aliquet. Integer in nibh urna. Quisque viverra nec ex commodo luctus. Vestibulum tempus libero eget vestibulum blandit. Suspendisse interdum lectus mattis consequat fermentum. Aenean volutpat feugiat mauris ac elementum.",
//                        image: UIImage(named: "profile-no-image"))
//        userService.value = user

        // Implement switch between onboarding and main screens

        userService.filter { $0 == nil }
            .with(.strong(router))
            .call { _, router in
                router.makeAsRootF(Onboarding1VM.self, inNavigation: true)
            }
            .dispose(in: reactive.bag)

        userService.filter { $0 != nil }
            .with(.strong(router))
            .call { data, router in
                router.makeAsRootF(MainVC.self, inNavigation: true, data: data!)
            }
            .dispose(in: reactive.bag)
    }
}
