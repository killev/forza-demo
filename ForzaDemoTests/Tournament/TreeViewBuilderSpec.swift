//
//  TreeViewBuilderSpec.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 9/10/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Quick
import Nimble

@testable import ForzaDemo

class TreeViewBuilderSpec: QuickSpec {
    override func spec() {
        describe("TreeViewBuilder") {
            context("Simple tree") {
                var value: String!
                var nodeLeft: BITreeNode<String>!
                var nodeRight: BITreeNode<String>!
                var node: BITreeNode<String>!

                beforeEach {
                    value = "NodeValue"
                    nodeLeft = BITreeNode<String>(value + "Left")
                    nodeRight = BITreeNode<String>(value + "Right")
                    node = BITreeNode<String>(value,
                                              left: nodeLeft,
                                              right: nodeRight)
                }

                it("Process one level node and generate one view") {
                    let builder = TreeViewBuilder<String, String>()
                    let res = builder.process(node: nodeLeft,
                                              createView: { ["View: \($0) \($1) \($2)"] },
                                              createConnections: { ["Connection: \($0) \($1) \($2)"] })
                    expect(res) == ["View: 0 0 NodeValueLeft"]
                }

                it("Process two level node and generate one 3 views and 1 connection") {
                    let builder = TreeViewBuilder<String, String>()
                    let res = builder.process(node: node,
                                              createView: { ["View: \($0) \($1) \($2)"] },
                                              createConnections: { ["Connection: \($0) \($1) \($2)"] })

                    expect(res).to(contain(["View: 1 1 NodeValue",
                                            "Connection: 0 2 0",
                                            "View: 0 0 NodeValueLeft",
                                            "View: 2 0 NodeValueRight"]))
                }
            }
        }
    }
}
