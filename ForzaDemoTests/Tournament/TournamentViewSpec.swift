//
//  TournamentViewSpec.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 9/9/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Quick
import Nimble

@testable import ForzaDemo

class TournamentViewSpec: QuickSpec {
    override func spec() {
        describe("Tournament View") {
            it("should be possible to create via frame") {
                let frame = CGRect(x: 0, y: 0, width: 100, height: 200)
                let tournamentView = TournamentView(frame: frame)
                expect(tournamentView.frame) == frame
            }
        }
    }
}
