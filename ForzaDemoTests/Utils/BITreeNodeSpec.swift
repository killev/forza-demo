//
//  BITreeNodeSpec.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 9/10/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Quick
import Nimble

@testable import ForzaDemo

class BITreeNodeSpec: QuickSpec {
    override func spec() {
        describe("BITreeNode") {
            it("should be possible to create with value") {
                let value = "NodeValue"
                let node = BITreeNode<String>(value)
                expect(node.value) == value
                expect(node.left).to(beNil())
                expect(node.right).to(beNil())
            }
            context("Two level tree") {
                var value: String!
                var nodeLeft: BITreeNode<String>!
                var nodeRight: BITreeNode<String>!
                var node: BITreeNode<String>!

                beforeEach {
                    value = "NodeValue"
                    nodeLeft = BITreeNode<String>(value + "Left")
                    nodeRight = BITreeNode<String>(value + "Right")
                    node = BITreeNode<String>(value,
                                              left: nodeLeft,
                                              right: nodeRight)
                }

                it("should be possible to create with value") {
                    expect(node.value) == value
                    expect(node.left).to(be(nodeLeft))
                    expect(node.right).to(be(nodeRight))
                }
                describe("depth") {
                    it("shourd return maximum depth of three") {
                        expect(node.depth) == 2
                        expect(SampleTree.tree.depth) == 3
                    }
                }
                describe("hasChildren") {
                    it("should return true if at least left or right not nil and false otherwise") {
                        expect(node.hasChildren) == true
                        expect(nodeLeft.hasChildren) == false
                    }
                }
            }
        }
    }
}
