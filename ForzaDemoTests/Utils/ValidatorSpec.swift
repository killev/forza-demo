//
//  ValidatorsSpec.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 9/3/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Quick
import Nimble

@testable import ForzaDemo

class ValidatorSpec: QuickSpec {
    //swiftlint: disable:next function_body_length
    override func spec() {
        describe("Validation") {
            describe("nameValidator") {
                let validator = Validator.nameValidator
                it("Should be negative if string shorter than 4 chars") {
                    expect(validator("al")) == false
                }
                it("Should be positive if string longer than or equal 4 chars") {
                    expect(validator("four")) == true
                    expect(validator("allfive")) == true
                }
                it("Should be negative if string contains digits or puncts") {
                    expect(validator("all123five")) == false
                    expect(validator("all[,]]five")) == false
                }
            }
            describe("phoneValidator") {
                let validator = Validator.phoneValidator
                it("Should be negative if string shorter or longer than 9 chars") {
                    expect(validator("12345678")) == false
                    expect(validator("1234567890")) == false
                }
                it("Should be positive if string equal to 9 chars and contains digits only") {
                    expect(validator("123456789")) == true
                }
                it("Should be negative if string contains digits or puncts") {
                    expect(validator("all123five")) == false
                    expect(validator("all[,]]five")) == false
                }
            }
            describe("passwordValidator") {
                let validator = Validator.passwordValidator
                it("password should be 6 or more characters") {
                    expect(validator("pass12")) == true
                    expect(validator("pass1")) == false
                }
            }
        }
    }
}
