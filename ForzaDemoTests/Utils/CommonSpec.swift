//
//  CommonSpec.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 9/10/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Quick
import Nimble

@testable import ForzaDemo

class CommonSpec: QuickSpec {
    override func spec() {
        describe("pow") {
            it("should return power of integers") {
                expect(pow(2, 2)) == 4
                expect(pow(2, 3)) == 8
                expect(pow(3, 2)) == 9
            }
        }
    }
}
