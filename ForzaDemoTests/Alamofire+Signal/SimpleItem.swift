//
//  SimpleItem.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 25.10.2019.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

struct SimpleObject {
    let location: String //"Partly cloudy"
    let forecast: [SimpleItem]
}

struct SimpleItem {
    let conditions: String //"Partly cloudy"
    let day: String //"Monday"
    let temperature: Float // 20
}

extension SimpleItem: Decodable {

//    init(map: Map) throws {
//        conditions = (try? map.value("conditions")) ?? ""
//        day = (try? map.value("day")) ?? ""
//        temperature = (try? map.value("temperature")) ?? 0
//    }
}

extension SimpleObject: Decodable {
    enum CodingKeys: String, CodingKey {
           case location
           case forecast = "three_day_forecast"
       }
//    init(map: Map) throws {
//        location = (try? map.value("location")) ?? ""
//        forecast = (try? map.value("three_day_forecast")) ?? []
//    }
}
