//
//  Alamofire+SignalSpec.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 25.10.2019.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Alamofire
import ReactiveKit

import Quick
import Nimble

class AlamofireResponseArraySpec: QuickSpec {
    //swiftlint:disable:next function_body_length
    override func spec() {

        describe("Alamofire") {
            describe("responseArray") {
                //swiftlint:disable:next line_length
                let url = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/master/sample_array_json"

                //swiftlint:disable:next line_length
                let mistakenUrl = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/master/sample_array_json1"
                var bag: DisposeBag!
                beforeEach {
                    bag = DisposeBag()
                }

                it("veryfy flatMapLatestChain") {

                    let signal = SafeSignal(sequence: [1, 2], interval: 5).eraseType()

                    let result = signal.flatMapLatest {
                        return Alamofire
                            .request(url,
                                     method: .get,
                                     parameters: nil,
                                     encoding: URLEncoding.default,
                                     headers: [:])
                            .responseArray(SimpleItem.self, keyPath: "")
                    }
                    var count = 0

                    result.observeNext { _ in
                        count += 1
                    }.dispose(in: bag)

                    expect(count).toEventually(be(2), timeout: 60)
                }

                it("Should retrive and map JSON to array of Immutable items") {

                    let result = Alamofire
                        .request(url,
                                 method: .get,
                                 parameters: nil,
                                 encoding: URLEncoding.default,
                                 headers: [:])
                        .responseArray(SimpleItem.self, keyPath: "")

                    waitUntil(timeout: 20) { done in
                        result.observeNext { items in
                            expect(items.count).to(be(3))
                            done()
                        }.dispose(in: bag)
                    }
                }

                it("Should handle alamofire error and turn them into Failure Stream") {

                    let result = Alamofire
                        .request(mistakenUrl,
                                 method: .get,
                                 parameters: nil,
                                 encoding: URLEncoding.default,
                                 headers: [:])
                        .responseArray(SimpleItem.self, keyPath: "")

                    waitUntil(timeout: 20) { done in
                        result.observeFailed { _ in
                            done()
                        }.dispose(in: bag)
                    }
                }
            }

            describe("responseObject") {
                var bag: DisposeBag!
                beforeEach {
                    bag = DisposeBag()
                }

                let url = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/master/sample_json"

                //swiftlint:disable:next line_length
                let mistakenUrl = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/master/sample_json1"

                it("Should retrive and map JSON to immutable object") {

                    let result = Alamofire
                        .request(url,
                                 method: .get,
                                 parameters: nil,
                                 encoding: URLEncoding.default,
                                 headers: [:])
                        .responseObject(SimpleObject.self, keyPath: "")

                    waitUntil(timeout: 20) { done in
                        result.observeNext { object in
                            expect(object.location) == "Toronto, Canada"//GOT from json
                            expect(object.forecast.count) == 3
                            done()
                        }.dispose(in: bag)
                    }
                }

                it("Should handle alamofire error and turn them into Failure Stream") {

                    let result = Alamofire
                        .request(mistakenUrl,
                                 method: .get,
                                 parameters: nil,
                                 encoding: URLEncoding.default,
                                 headers: [:])
                        .responseObject(SimpleObject.self, keyPath: "")

                    waitUntil(timeout: 20) { done in
                        result.observeFailed { _ in
                            done()
                        }.dispose(in: bag)
                    }
                }
            }
        }
    }
}
