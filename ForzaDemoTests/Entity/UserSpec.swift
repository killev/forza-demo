//
//  UserSpec.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 9/10/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Quick
import Nimble

@testable import ForzaDemo

class UserSpec: QuickSpec {
    override func spec() {
        describe("user") {
            describe("patch") {
                it("should return new user with updated patched fields") {
                    let user = User(firstName: "",
                                    lastName: "",
                                    city: "",
                                    email: "",
                                    password: "",
                                    note: "",
                                    image: nil)

                    let patched = user.patch(firstName: "firstName",
                                             lastName: "lastName",
                                             city: "city",
                                             email: "email",
                                             password: "password",
                                             note: "note",
                                             image: UIImage())

                    expect(patched.firstName) == "firstName"
                    expect(patched.lastName) == "lastName"
                    expect(patched.city) == "city"
                    expect(patched.email) == "email"
                    expect(patched.password) == "password"
                    expect(patched.note) == "note"
                    expect(patched.image).notTo(beNil())
                }
            }
        }
    }
}
