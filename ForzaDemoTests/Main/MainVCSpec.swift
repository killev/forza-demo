//
//  MainVCSpec.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 9/1/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Quick
import Nimble

import Swinject
import SwinjectAutoregistration
import ReaktiveBoar
import BrightFutures
import Bond
@testable import ForzaDemo

class MainVCSpec: QuickSpec {
    override func spec() {
        var context: Container!
        var viewController: MainVC!

        beforeEach {
            ReaktiveBoar.setup()
            context = Container()
            context.register(value: User(firstName: "First",
                                         lastName: "Last",
                                         city: "London",
                                         email: "email@delete.com",
                                         password: "password",
                                         note: "note",
                                         image: UIImage()))

            context.register(RouterProtocol1.self) { resolver in
                return Router1(parent: resolver)
            }.inObjectScope(.container)
            let router: RouterProtocol1 = context~>

            router.makeAsRootF(MainVC.self, inNavigation: true).onSuccess { vc in
                viewController = vc as? MainVC
            }
            expect(viewController).toEventuallyNot(beNil(), timeout: 50)

            viewController.beginAppearanceTransition(true, animated: false)
            viewController.endAppearanceTransition()
        }

        afterEach {
            viewController.viewWillDisappear(false)
            viewController.viewDidDisappear(false)
            viewController = nil
        }

        it("Should have ProfileVC and TournamentVC tabs") {
            expect(viewController.children.count) == 2
            expect(viewController.children[0]).to(beAnInstanceOf(ProfileVC.self))
            expect(viewController.children[1]).to(beAnInstanceOf(TournamentVC.self))
        }
    }
}
