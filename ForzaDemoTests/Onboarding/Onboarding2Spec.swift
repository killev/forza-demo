//
//  Onboarding2Spec.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 9/1/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Quick
import Nimble

import Swinject
import SwinjectAutoregistration
import ReaktiveBoar
import BrightFutures
import ReactiveKit
import Bond
@testable import ForzaDemo

class Onboarding2Spec: QuickSpec {
    override func spec() {
        var context: Container!
        var viewController: Onboarding2VC!

        beforeEach {
            ReaktiveBoar.setup()
            context = Container()
            context.register(value: Property<User?>(nil)).inObjectScope(.container)
            context.register(RouterProtocol1.self) { resolver in
                return Router1(parent: resolver)
            }.initCompleted { _, router in
                router.register(Onboarding2VC.self)
            }.inObjectScope(.container)
            let router: RouterProtocol1 = context~>

            let user = User(firstName: "first",
                            lastName: "lasr",
                            city: "city",
                            email: "email@delete.com",
                            password: "password",
                            note: "",
                            image: nil)
            router.makeAsRootF(Onboarding2VC.VMType.self, inNavigation: true, data: user).onSuccess { vc in
                viewController = vc as? Onboarding2VC
            }
            expect(viewController).toEventuallyNot(beNil(), timeout: 50)

            viewController.beginAppearanceTransition(true, animated: false)
            viewController.endAppearanceTransition()
        }

        afterEach {
            viewController.viewWillDisappear(false)
            viewController.viewDidDisappear(false)
            viewController = nil
        }

        it("should be inside navigation hierarchy") {
            expect(viewController.navigationController).notTo(beNil())
        }

        it("should next button") {
            expect(viewController.register).notTo(beNil())
            expect(viewController.register.isEnabled) == false
        }

        it("should open step 2 on next button") {

            viewController.register
                .sendActions(for: .touchUpInside)

            justSleep(.seconds(1))

            let user: Property<User?> = context~>

            expect(user.value)
                .notTo(beNil())
        }
    }
}
