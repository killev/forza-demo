//
//  EditCellSpec.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 9/3/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Quick
import Nimble

import Swinject
import SwinjectAutoregistration
import ReaktiveBoar
import BrightFutures
import ReactiveKit
import Bond
@testable import ForzaDemo

//class EditCellSpec: QuickSpec {
//    override func spec() {
//
//        var bag: DisposeBag!
//        var cell: EditCell!
//        var collectionView = UICollectionView()
//        collectionView.register(EditCell.self, forCellWithReuseIdentifier: EditCell.className)
//        beforeEach {
//            bag = DisposeBag()
//            cell = collectionView
//                .dequeueReusableCell(withReuseIdentifier: EditCell.className, for: IndexPath(0,0))
//        }
//
//        describe("EditCell Specification") {
//            it("Sould adise ") {
//                let vm = EditCell.VMType(prefix: "Prefix",
//                                         initText: "Placeholder",
//                                         formatter: nil,
//                                         keyboardType: UIKeyboardType.decimalPad,
//                                         isSecureTextEntry: false,
//                                         charValidator: {_ in true },
//                                         validator: { _ in true })
//
//
//                cell.advise(vm: vm, bag: bag)
//                expect(cell.edit.placeholder) == vm.initText
//                expect(cell.prefix.text) == vm.prefix
//            }
//        }
//    }
//}
