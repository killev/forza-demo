//
//  Onboarding1Spec.swift
//  ForzaDemoTests
//
//  Created by Peter Ovchinnikov on 8/31/19.
//  Copyright © 2019 BoarLabs. All rights reserved.
//

import Quick
import Nimble

import Swinject
import SwinjectAutoregistration
import ReaktiveBoar
import BrightFutures
import ReactiveKit
import Bond
@testable import ForzaDemo

class Onboarding1Spec: QuickSpec {
    //swiftlint:disable:next function_body_length
    override func spec() {
        var context: Container!
        var viewController: Onboarding1VC!

        beforeEach {
            ReaktiveBoar.setup()
            context = Container()
            context.register(value: Property<User?>(nil))
            context.register(RouterProtocol1.self) { resolver in
                return Router1(parent: resolver)
            }.initCompleted { _, router in
                router.register(Onboarding1VC.self)
                router.register(Onboarding2VC.self)
            }.inObjectScope(.container)
            let router: RouterProtocol1 = context~>

            router.makeAsRootF(Onboarding1VC.VMType.self, inNavigation: true).onSuccess { vc in
                viewController = vc as? Onboarding1VC
            }
            expect(viewController).toEventuallyNot(beNil(), timeout: 50)

            viewController.beginAppearanceTransition(true, animated: false)
            viewController.endAppearanceTransition()
        }

        afterEach {
            viewController.viewWillDisappear(false)
            viewController.viewDidDisappear(false)
            viewController = nil
        }

        it("should be inside navigation hierarchy") {
            expect(viewController.navigationController).notTo(beNil())
        }

        it("should have next button") {
            expect(viewController.nextScreen).notTo(beNil())
            expect(viewController.collectionView).notTo(beNil())
            expect(viewController.nextScreen.isEnabled) == false
        }

        it("should have 7 fields") {

            let count = viewController.collectionView.numberOfItems(inSection: 0)
            let cell = viewController.collectionView.cellAtRow(EditCell.self, row: 0)

            expect(count) == 7
            expect(cell).to(beAnInstanceOf(EditCell.self))
            expect(cell.edit.placeholder) == "First Name"
        }

        it("verify first name") {

            let cell = viewController.collectionView.cellAtRow(EditCell.self, row: 0)
            expect(cell.edit.placeholder) == "First Name"
            expect(cell.prefix.text) == ""
            let vm: Onboarding1VM = viewController.resolver~>

            expect(vm.dataSource.value.count) > 0

            //swiftlint1:disable:next force_cast
//            let cellVM = vm.dataSource.value[0] as! EditCellVM
//            cellVM.text.observeNext {
//                print("Text: \($0)")
//            }
//
//            let range = cell.edit.textRange(from: cell.edit.beginningOfDocument,
//                                            to: cell.edit.endOfDocument)
//            let replacementText = "firstName"

//
//            if cell.edit.de.shouldChangeText(in: range!, replacementText: replacementText) {
//                cell.edit.replace(range!, withText: "firstName")
//            }
            justSleep(.milliseconds(500))
        }

        it("should open step 2 on next button") {

            viewController.nextScreen
                .sendActions(for: .touchUpInside)

            justSleep(.seconds(1))

            expect(viewController.navigationController?.viewControllers.last)
                .to(beAnInstanceOf(Onboarding2VC.self))
        }
    }
}

extension UICollectionView {
    func cellAtRow<T: UICollectionViewCell>(_ type: T.Type, row: Int) -> T {
        if let typedCell = cellForItem(at: IndexPath(row: row, section: 0)) as? T {
            return typedCell
        }
        fatalError("")
    }
}

func justSleep(_ timeout: DispatchTimeInterval, file: FileString = #file, line: UInt = #line) {
    waitUntil(timeout: timeout.toDouble() + 1, file: file, line: line) { done in
        Future<Void, Never>(value: (), delay: timeout).onSuccess(callback: done)
    }
}

extension DispatchTimeInterval {
    func toDouble() -> Double {
        var result: Double

        switch self {
        case .seconds(let value):
            result = Double(value)
        case .milliseconds(let value):
            result = Double(value) * 0.001
        case .microseconds(let value):
            result = Double(value) * 0.000001
        case .nanoseconds(let value):
            result = Double(value) * 0.000000001

        case .never:
            result = 0
        default:
            fatalError()
        }

        return result
    }
}
