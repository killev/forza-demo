## Environment
### 1. Use gitlab over github

Both gitlab and github provide full control over the code. But gitlab has CI system, better issue tracker, and merge requests out of the box, what makes starting the project easier. That's why we're here!

### 2. Automate you delivery process using Fastlane
Fastlane is a popular and very simple tool, writtin in ruby. 
It can build, test and deliver your application to users.

The last is one of the most regular task. Automate it to save your time and avoid pain.

There're a couple of services for delivering your app to different groups of people:

1. [Fabric Beta](https://fabric.io/home/) - which is goind to become a part of Google Firebase
2. [AppCenter](https://appcenter.ms/) - former HokeyApp, aquired by Microsoft
3. [Testflight](https://developer.apple.com/testflight/) (Service from Apple)

You may use any for test purpose but Testflight is the only one appropriate for final releases.

### 3. Use swiftlint to spread style across all team and project

People are different and forgetful. Swiftlint - no. 
It will correct you every time you build you code. So you'll find mistake as soon at it appears.

### 4. Automate **strict** style using Fastlane

It's OK to have stylish warnings or ever error during development process. 

But only code with no warning is allowed to be merged into master. Use **Strict** mode in Fastlane.

### 5. Automate tests run using Fastlane

Unit and UI tests give you a vision where you're now and how much stable and predictable your code is.
Only code with all tests passed is allowed to be merged into master.

### 6. Automate coverage check using Fastlane 

The more code is covered by tests the higher quality of the product. 
Only code covered by tests is allowed to be merged into master.
Use **coverage check** mode in Fastlane.

### 7. Simplify branches flow using gitlab's merge requests.

I suggest using 2 kind of branches only:
- master - for stable version 
- 33-issue-branch - branch for specific issue. 

Gitlab automates creating the issues' branches, and merging process. Along with other automatic checks, merging becomes safe enough.

### 8. Setup gitlab's ci process:

**master** branch: `Prepare -> Check Style ->  Run Tests -> Publish to product (Testflight)`

**feature** branch: `Prepare -> Check Style ->  Run Tests -> Publish to test (Fabric or Crashlytics)`



<details>
<summary>For example running tests looks like this:</summary>

```ruby
  lane :tests do
  
    run_tests(
      scheme: gym_scheme,
      clean: true,
      code_coverage: true
    )

    res = xcov(
      scheme: gym_scheme,
      output_directory: "xcov_output",
      json_report: true,
      html_report: false,
      markdown_report: true,
      minimum_coverage_percentage: 90.0,
	)
	
    file = File.read('../xcov_output/report.json')
    report = JSON.parse(file)
    coverage = report["coverage"].to_f()
    UI.message "TOTAL Coverage: #{ to_percent(coverage) }%"
  end
```
</details>

### 5. Check code coverage as a pipeline step.
[coveralls.io](http://coveralls.io) and codecov.io

### 6. Use .gitignore to avoid repository from generated files.

### 7. Use push rules to make sure your add right commit message.

This regexp force you to add number of issue you're working on. branch will be deleted, issue will persist in the tracker.

`(Bump Version)|((?i:Wip|Done|Fix|Closes|Fixes) #\d+)`

### 8. Use linter.

swiftlint 


Environment plays iportant role in quality of software. It 


