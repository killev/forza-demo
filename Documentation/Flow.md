## Flow

It's a good practive to have code covered by tests which're running constantly. Ther every commit.
  - Continious tests running and coverage check
  - Continious Deploy Cycle
  - Continious Releases

```mermaid
graph LR;
  Branch --> Code --> Style(Static Analysys) --> Commit --> Test --> Review --> Build(Test Build and deploy) --> Merge --> Release(Release build Deploy) --> Publish(Publish to AppStore);
```

```mermaid
graph TB
    stable1(Stable Master)-->
    coding(Coding)-->staticCheck(Linting)
    staticCheck-.->coding
    staticCheck-->commit(Commit)
    commit-->tests
    tests-.->coding
    tests(Unit and UI tests)-->|Release Notes for Tester|build
    tests-->review
    build(Internal Build)-->verification
    verification(Manual Verification)-->merge
    review(Code Review)-.->coding
    verification-.->coding
    review-->merge(Merging into master)-->stable2(Stable Master)
```


