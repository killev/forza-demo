## Code

  - Style
  - Lack of dangerous construction
    - avoid [unowned ] construction
    - avoid implicit unwrap (`var x:Type!`) unless this is required by Framework
  - Small classes and function
  - Clear names

## Code

### 1. Use linter to keep your code clean and readable

It makes everyone to keep the same style rules over all the project.

### 2. Use linter to avoid committing focused tests, cases, contexts

### 3. Use linter to avoid dangerous constructions like:
- unowned closure captures
- implicitly unwrapped properties

### 4. Prefer BDD over TDD.

Quick and Nimble

### 5. Prefer let over var in class properlies

And never use optional as class properties.

<details>
<summary>Bad example</summary>

```swift
class BadClassDesign {
    var model: Model?
    init() {
        
    }
    
    func applyModel() {
        guard let model = model else {
            fatalError("We shouldn't be here")
        }
        ...
        // use model
    }
    
}
...
badClass.model = Model()
badClass.applyModel()
...
```

</details>

<details>
<summary>Good example</summary>

```swift
class GoodClassDesign {
    let model: Model
    init(model: Model) {
        self.model = model
    }
    func applyModel() {
        // use model
    }
}
...
goodClass = GoodClassDesign(model: Model())
goodClass.applyModel()
...

```
</details>