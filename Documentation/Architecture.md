## Architecture

### Stateless
### Global Managed State
### MVVM
### Functional / Declarative 
### Super
### Managed state

## Architecture
### 1. Prefer architecture frameworks over implementing architecture ourselves
### 2. Don't use abstract classes untill you need this

The
```swift

protocol WriteToFileProtocol {
    func writeToFile(file: File)
}

class WriteToFile {
    init(data: Data) {
        
    }
    func writeToFile(file: File) {
        
    }
}

```


Don't care about Clean Architecture at early stage. 
You never guess where you really need the abstraction.

### 3. Use concrete class until you need the abstraction